import ReactDOM from 'react-dom'
import React from 'react'
import PopupContainer from './popup/popup-container';
import popupCreator from './popup/popup-creator';

(function (helper) {
    window.IMSPlugins = window.IMSPlugins || {};
    window.IMSPlugins.TransformHelper = helper();

    const initCheckFn = function () {
        let divEl = document.getElementById('IMSPlugin-PopupContainer');
        if (divEl == null || typeof divEl == 'undefined') {
            //create overlay element
            let overlayEl = document.createElement("div");
            overlayEl.id = 'IMSPugins_Overlay';
            overlayEl.className = 'IMSPlugin-Overlay';
            
            //create popup container element
            divEl = document.createElement("div");
            divEl.id = 'IMSPlugin-PopupContainer';
            divEl.className = 'IMSPlugin-PopupContainer';
            document.body.appendChild(divEl);
            document.body.appendChild(overlayEl);
        }

        ReactDOM.render(<PopupContainer />, divEl);
    };

    if (document.readyState == "complete" || document.readyState == "loaded") {
        initCheckFn();
    }
    else {
        window.onload = initCheckFn;
    }

})(function () {
    const helper = {
        showPopup: function (reactComponent, props) {
            popupCreator.showPopup(reactComponent, props);
        },

        closePopup: function (reactComponent) {
            popupCreator.closePopup(reactComponent);
        }

    };

    return helper;
});