import React, { Component } from 'react';
import popupStore from './popup-store';
import PropTypes from 'prop-types';

class PopupItem extends Component {
    componentDidMount() {
        const self = this;
        //load init data
        setTimeout(function () {
            const pop = self.refs[self.props.refName];
            if (pop) {
                pop.addClass('show');
            }
        }, 300);

        self.addListenerTransitionEnd();
    }

    componentWillUnmount() {
        this.removeListenerTransitionEnd();
    }

    render() {
        const self = this;
        return (
            <div className="IMSPlugin-PopupItem" style={{zIndex: self.props.zIndex}} ref={function (el) {
                self.refs[self.props.refName] = el;
            }}>
                {this.props.children}
            </div>
        );
    }

    detectTransitionEndEvent = () => {
        var t;
        var el = document.createElement('fakeelement');
        var transitions = {
            'Transition': 'transitionend',
            'OTransition': 'otransitionend',
            'MozTransition': 'transitionend',
            'WebkitTransition': 'webkitTransitionEnd',
            'MSTransition': 'MSTransitionEnd'
        };

        for (t in transitions) {
            if (el.style[t] !== undefined) {
                return transitions[t];
            }
        }

        return 'transitionend';
    }

    addListenerTransitionEnd = () => {
        const self = this;
        const endEvent = self.detectTransitionEndEvent();
        if (endEvent && self.refs[self.props.refName]) {
            self.refs[self.props.refName].addEventListener(endEvent, self.onTransitionEnd, false);
        }
    }

    removeListenerTransitionEnd = () => {
        const self = this;
        const endEvent = self.detectTransitionEndEvent();
        if (endEvent && self.refs[self.props.refName]) {
            self.refs[self.props.refName].removeEventListener(endEvent, self.onTransitionEnd, false);
        }
    }

    onTransitionEnd = event => {
        if (event.target.className.indexOf("IMSPlugin-PopupItem") !== -1 && event.propertyName.indexOf("transform") !== -1) {
            //Sử dụng css popup
            if (event.target.className.indexOf("show") === -1) {
                popupStore.emitPopupDidClose();
            }
            else {
                popupStore.emitPopupDidShow();
            }
        }
    }
}

PopupItem.propTypes = {
    zIndex: PropTypes.number,
    children: PropTypes.oneOfType([
        PropTypes.array,
        PropTypes.element
    ]).isRequired
}

export default PopupItem;


