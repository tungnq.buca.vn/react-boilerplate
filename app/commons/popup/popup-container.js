import React, { Component } from 'react';
import popupStore from './popup-store';
import PopupItem from './popup-item';


class PopupContainer extends Component {
    constructor(props) {
        super(props);
        this._zIndex = this._getMaxZIndex();
        this._isOpen = false;
        this.state = {
            isMounted: false,
            listPopup: []
        }
    }

    componentDidMount() {
        this.setState({ isMounted: true})
        popupStore.addOpenPopupListener(this._onOpenPopup);
        popupStore.addClosePopupListener(this._onClosePopup);
        popupStore.addPopupDidShowListener(this._onPopupDidShow);
        popupStore.addPopupDidCloseListener(this._onPopupDidClose);
    }

    componentWillUnmount() {
        popupStore.removeOpenPopupListener(this._onOpenPopup);
        popupStore.removeClosePopupListener(this._onClosePopup);
        popupStore.removePopupDidShowListener(this._onPopupDidShow);
        popupStore.removePopupDidCloseListener(this._onPopupDidClose);
    }

    _onOpenPopup = () => {
        const data = popupStore.getCurrentPopupData();
        this._isOpen = true;
        this.updatePopupData(data);
        var wrapOverlay = document.getElementById('IMSPugins_Overlay')
        document.getElementById('IMSPugins_Overlay').classList.add('show');
    }

    _onClosePopup = () => {
        const self = this;
        const data = popupStore.getCurrentPopupData();
        if (typeof data.component != 'undefined') {
            const displayName = data.component.displayName ? data.component.displayName : data.component.name;
            const _ref = "popup_" + displayName;
            const pop = self.refs[_ref];
            if (pop) {
                pop.removeClass('show');
            }
        }
    }

    _getMaxZIndex = () =>{
        try {
            var max = Array.from(document.querySelectorAll('body *'))
            .map(a => parseFloat(window.getComputedStyle(a).zIndex))
            .filter(a => !isNaN(a))
            .sort()
            .pop();

            if(max < 500) max = 500;

            return max;

        } catch (error) {
            return 500;
        }
    }

    _onPopupDidShow = () => {
        this._isOpen = false;
    }

    /*Xử lý sự kiện sau khi đã đóng popup (chạy animation xong)*/
    _onPopupDidClose = () => {
        let list = this.state.listPopup;

        list.pop();
        this.setState({listPopup: list},function(){
            // console.log("popup did close");
            if (this.state.listPopup.length == 0) {
                document.getElementById('IMSPugins_Overlay').removeClass('show');
            }
        });
    }

    updatePopupData = newItem => {
        const list = this.state.listPopup;
        const existItem = this._checkExist(newItem);

        if (!existItem) {
            list.push(newItem);

            if (this.state.isMounted) {
                this.setState({listPopup: list});
            }
        }
    }

    _checkExist = newItem => {
        const list = this.state.listPopup;
        const newItemName = newItem.component.displayName ? newItem.component.displayName : newItem.component.name;
        list.forEach(item => {
            const displayName = item.component.displayName ? item.component.displayName : item.component.name;
            if (displayName === newItemName) {
                return true;
            }
        });

        return false;
    }

    renderItems = () => {
        const self = this;
        let zIndex = self._zIndex;
        let refName = "";
        if (self.state.listPopup.length > 0) {
            var renderItem = function (item, index) {
                var props = Object.assign({},item.opts,{ key: "comp_" + index})
                return <item.component {...props} />;
            };

            return self.state.listPopup.map(function (item, i) {
                if (item.component) {
                    zIndex = self._zIndex + i + 1;
                    const displayName = item.component.displayName ? item.component.displayName
                                                                    : item.component.name;
                    refName = "popup_" + displayName;
                    return (
                        <PopupItem key={i} zIndex={zIndex} refName={refName} >
                            {renderItem(item, i)}
                        </PopupItem>
                    );
                }
                else {
                    return null;
                }

            });

        }
        return null;
    }

    render() {
        return (
            <div className="popup-container">
                {
                    this.renderItems()
                }
            </div>
        );
    }
}

export default PopupContainer;