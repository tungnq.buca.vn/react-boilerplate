const actionTypes = {
    OPEN_POPUP : 'OPEN_POPUP',
    CLOSE_POPUP : 'CLOSE_POPUP'
};

export default actionTypes;