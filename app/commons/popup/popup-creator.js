import pluginDispatcher from '../plugin-dispatcher';
import actionTypes from './popup-action-types';

const popupCreator = {
    showPopup : function(component,opts){
        pluginDispatcher.dispatch({
            action: actionTypes.OPEN_POPUP,
            component: component,
            opts: opts
        });
    },
    closePopup: function (component) {
        pluginDispatcher.dispatch({
            action: actionTypes.CLOSE_POPUP,
            component: component
        });
    }
};

export default popupCreator;