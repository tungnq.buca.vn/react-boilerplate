import { EventEmitter } from 'events';
import pluginDispatcher from '../plugin-dispatcher';
import actionTypes from './popup-action-types';

let _currentPopData = null;
const POPUP_DID_SHOW = "POPUP_DID_SHOW";
const POPUP_DID_CLOSE = 'POPUP_DID_CLOSE';
const OPEN_POPUP_EVENT = 'OPEN_POPUP_EVENT';
const CLOSE_POPUP_EVENT = 'CLOSE_POPUP_EVENT';

const PopupStore = Object.assign({}, EventEmitter.prototype, {
    emitPopupDidClose: function () {
        this.emit(POPUP_DID_CLOSE);
    },
    addPopupDidCloseListener: function (callback) {
        this.on(POPUP_DID_CLOSE, callback);
    },
    removePopupDidCloseListener: function (callback) {
        this.removeListener(POPUP_DID_CLOSE, callback);
    },

    emitPopupDidShow: function () {
        this.emit(POPUP_DID_SHOW);
    },
    addPopupDidShowListener: function (callback) {
        this.on(POPUP_DID_SHOW, callback);
    },
    removePopupDidShowListener: function (callback) {
        this.removeListener(POPUP_DID_SHOW, callback);
    },

    emitOpenPopup: function () {
        this.emit(OPEN_POPUP_EVENT);
    },
    addOpenPopupListener: function (callback) {
        this.on(OPEN_POPUP_EVENT, callback);
    },
    removeOpenPopupListener: function (callback) {
        this.removeListener(OPEN_POPUP_EVENT, callback);
    },

    emitClosePopup: function () {
        this.emit(CLOSE_POPUP_EVENT);
    },
    addClosePopupListener: function (callback) {
        this.on(CLOSE_POPUP_EVENT, callback);
    },
    removeClosePopupListener: function (callback) {
        this.removeListener(CLOSE_POPUP_EVENT, callback);
    },
    
    getCurrentPopupData: function () {
        return _currentPopData;
    }
});

PopupStore.dispatchToken = pluginDispatcher.register(function (payload) {

    switch (payload.action) {
        
        case actionTypes.OPEN_POPUP:
        {
            _currentPopData  = {
                component: payload.component,
                opts: payload.opts
            };            

            PopupStore.emitOpenPopup();
            
            break;
        }
        
        case actionTypes.CLOSE_POPUP:
        {
            _currentPopData  = {
                component: payload.component,
                opts: {}
            };

            PopupStore.emitClosePopup();
            
            break;
        }
    }
});

export default PopupStore;