import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import { Dialog } from '@material-ui/core';
import safeInvoke from "utils/safeInvoke";


export default class PopupTrigger extends Component {
    static propTypes = {
        children: PropTypes.func.isRequired,
        renderPopup: PropTypes.func.isRequired,
        onMount: PropTypes.func,
    }

    state = {
        open: false
    }

    handleClose = () => {
        this.setState({open: false});
    }

    handleOpen = () => {
        this.setState({open: true});
    }

    componentDidMount() {
        safeInvoke(this.props.onMount, {
            onOpen: this.handleOpen
        })
    }

    render() {
        const {children, renderPopup, ...popupOptions} = this.props;
        const {handleOpen, handleClose} = this;
        return (
            <Fragment>
                {children({handleOpen, onOpen: handleOpen, handleClose, onClose: handleClose})}
                <Dialog disableEnforceFocus
                        open={this.state.open}
                        onClose={this.handleClose}
                        disableEscapeKeyDown={true}
                    /*TransitionComponent={Transition}*/
                        className="imsd-dialog"
                        {...popupOptions}>
                    {renderPopup({handleOpen, onOpen: handleOpen, handleClose, onClose: handleClose})}
                </Dialog>
            </Fragment>
        )
    }
}
