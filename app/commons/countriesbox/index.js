import React from 'react';
import ReactCustomFlagSelect from 'react-custom-flag-select';
import "react-custom-flag-select/lib/react-custom-flag-select.min.css";
import './css.scss'

const find = (arr, obj) => {
  const res = [];
  arr.forEach(o => {
    let match = false;
    Object.keys(obj).map(i => {
      if (obj[i] == o[i]) {
        match = true;
      }
    });
    if (match) {
      res.push(o);
    }
  });
  return res;
};




export default class Box extends React.Component {
  constructor(props){
    super(props)
    this.state={
      countries : [...initCountries],
      areaCode: 1,
      phone: 0,
      validate: true,
      isActive : false
    }
  }
  setIsActive = () =>{
    this.setState({
      isActive: true
    })
  }
  render() {
    const { areaCode, isActive, phone, validate } = this.state;
    const currentItem = find(this.state.countries, { id: areaCode })[0];
    var classActive  = isActive ? 'active' : ''
    return (
      <ReactCustomFlagSelect
        tabIndex={'1'} //Optional.[String or Number].Default: -1.
        id={'areaCode'} //Optional.[String].Default: "". Input ID.
        name={'areaCode'} //Optional.[String].Default: "". Input name.
        value={currentItem.id} //Optional.[String].Default: "".
        disabled={false} //Optional.[Bool].Default: false.
        showArrow={true} //Optional.[Bool].Default: true.
        animate={true} //Optional.[Bool].Default: false.
        optionList={this.state.countries} //Required.[Array of Object(s)].Default: [].
        // selectOptionListItemHtml={<div>us</div>} //Optional.[Html].Default: none. The custom select options item html that will display in dropdown list. Use it if you think the default html is ugly.
        // selectHtml={<div>us</div>} //Optional.[Html].Default: none. The custom html that will display when user choose. Use it if you think the default html is ugly.
        classNameWrapper="" //Optional.[String].Default: "".
        classNameContainer="" //Optional.[String].Default: "".
        classNameOptionListContainer={classActive + " buca-list-countrie"} //Optional.[String].Default: "".
        classNameOptionListItem="" //Optional.[String].Default: "".
        classNameDropdownIconOptionListItem="" //Optional.[String].Default: "".
        customStyleWrapper={{}} //Optional.[Object].Default: {}.
        customStyleContainer={{ border: 'none', fontSize: '12px' }} //Optional.[Object].Default: {}.
        customStyleSelect={{ width: '100%' }} //Optional.[Object].Default: {}.
        customStyleOptionListItem={{}} //Optional.[Object].Default: {}.
        customStyleOptionListContainer={{ maxHeight: '200px', overflow: 'auto', width: '100%', marginTop: '11px' }} //Optional.[Object].Default: {}.
       onChange={areaCode => {
         this.setState({ areaCode: areaCode },()=>{
          this.props.onChange && this.props.onChange(areaCode)
         });
       }}
      // onBlur={() => {}} //Optional.[Func].Default: none.
      // onFocus={(e) => {console.log(e)}} //Optional.[Func].Default: none.
      onClick={(e) => this.setIsActive()} //Optional.[Func].Default: none.
      />
    )
  }
}
const initCountries = [
  { code: 'AD', displayText: 'Andorra', id: '376' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AE', displayText: 'United Arab Emirates', id: '971' , locale: 'Andorra', flag: require('./images/united arab emirates.png')},
  { code: 'AF', displayText: 'Afghanistan', id: '93' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AG', displayText: 'Antigua and Barbuda', id: '1-268' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AI', displayText: 'Anguilla', id: '1-264' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AL', displayText: 'Albania', id: '355' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AM', displayText: 'Armenia', id: '374' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AO', displayText: 'Angola', id: '244' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AQ', displayText: 'Antarctica', id: '672' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AR', displayText: 'Argentina', id: '54' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AS', displayText: 'American Samoa', id: '1-684' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AT', displayText: 'Austria', id: '43' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AU', displayText: 'Australia', id: '61', suggested: true , flag: require('./images/1.jpg')},
  { code: 'AW', displayText: 'Aruba', id: '297' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AX', displayText: 'Alland Islands', id: '358' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'AZ', displayText: 'Azerbaijan', id: '994' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BA', displayText: 'Bosnia and Herzegovina', id: '387' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BB', displayText: 'Barbados', id: '1-246' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BD', displayText: 'Bangladesh', id: '880' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BE', displayText: 'Belgium', id: '32' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BF', displayText: 'Burkina Faso', id: '226' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BG', displayText: 'Bulgaria', id: '359' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BH', displayText: 'Bahrain', id: '973' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BI', displayText: 'Burundi', id: '257' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BJ', displayText: 'Benin', id: '229' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BL', displayText: 'Saint Barthelemy', id: '590' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BM', displayText: 'Bermuda', id: '1-441' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BN', displayText: 'Brunei Darussalam', id: '673' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BO', displayText: 'Bolivia', id: '591' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BR', displayText: 'Brazil', id: '55' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BS', displayText: 'Bahamas', id: '1-242' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BT', displayText: 'Bhutan', id: '975' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BV', displayText: 'Bouvet Island', id: '47' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BW', displayText: 'Botswana', id: '267' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BY', displayText: 'Belarus', id: '375' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'BZ', displayText: 'Belize', id: '501' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CA', displayText: 'Canada', id: '1', suggested: true , flag: require('./images/1.jpg')},
  { code: 'CC', displayText: 'Cocos (Keeling) Islands', id: '61' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CD', displayText: 'Congo, Democratic Republic of the', id: '243' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CF', displayText: 'Central African Republic', id: '236' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CG', displayText: 'Congo, Republic of the', id: '242' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CH', displayText: 'Switzerland', id: '41' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CI', displayText: "Cote d'Ivoire", id: '225' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CK', displayText: 'Cook Islands', id: '682' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CL', displayText: 'Chile', id: '56' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CM', displayText: 'Cameroon', id: '237' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CN', displayText: 'China', id: '86' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CO', displayText: 'Colombia', id: '57' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CR', displayText: 'Costa Rica', id: '506' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CU', displayText: 'Cuba', id: '53' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CV', displayText: 'Cape Verde', id: '238' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CW', displayText: 'Curacao', id: '599' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CX', displayText: 'Christmas Island', id: '61' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CY', displayText: 'Cyprus', id: '357' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'CZ', displayText: 'Czech Republic', id: '420' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'DE', displayText: 'Germany', id: '49', suggested: true , flag: require('./images/1.jpg')},
  { code: 'DJ', displayText: 'Djibouti', id: '253' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'DK', displayText: 'Denmark', id: '45' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'DM', displayText: 'Dominica', id: '1-767' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'DO', displayText: 'Dominican Republic', id: '1-809' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'DZ', displayText: 'Algeria', id: '213' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'EC', displayText: 'Ecuador', id: '593' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'EE', displayText: 'Estonia', id: '372' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'EG', displayText: 'Egypt', id: '20' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'EH', displayText: 'Western Sahara', id: '212' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ER', displayText: 'Eritrea', id: '291' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ES', displayText: 'Spain', id: '34' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ET', displayText: 'Ethiopia', id: '251' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'FI', displayText: 'Finland', id: '358' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'FJ', displayText: 'Fiji', id: '679' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'FK', displayText: 'Falkland Islands (Malvinas)', id: '500' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'FM', displayText: 'Micronesia, Federated States of', id: '691' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'FO', displayText: 'Faroe Islands', id: '298' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'FR', displayText: 'France', id: '33', suggested: true , flag: require('./images/1.jpg')},
  { code: 'GA', displayText: 'Gabon', id: '241' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GB', displayText: 'United Kingdom', id: '44' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GD', displayText: 'Grenada', id: '1-473' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GE', displayText: 'Georgia', id: '995' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GF', displayText: 'French Guiana', id: '594' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GG', displayText: 'Guernsey', id: '44' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GH', displayText: 'Ghana', id: '233' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GI', displayText: 'Gibraltar', id: '350' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GL', displayText: 'Greenland', id: '299' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GM', displayText: 'Gambia', id: '220' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GN', displayText: 'Guinea', id: '224' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GP', displayText: 'Guadeloupe', id: '590' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GQ', displayText: 'Equatorial Guinea', id: '240' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GR', displayText: 'Greece', id: '30' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GS', displayText: 'South Georgia and the South Sandwich Islands', id: '500' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GT', displayText: 'Guatemala', id: '502' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GU', displayText: 'Guam', id: '1-671' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GW', displayText: 'Guinea-Bissau', id: '245' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'GY', displayText: 'Guyana', id: '592' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'HK', displayText: 'Hong Kong', id: '852' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'HM', displayText: 'Heard Island and McDonald Islands', id: '672' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'HN', displayText: 'Honduras', id: '504' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'HR', displayText: 'Croatia', id: '385' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'HT', displayText: 'Haiti', id: '509' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'HU', displayText: 'Hungary', id: '36' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ID', displayText: 'Indonesia', id: '62' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IE', displayText: 'Ireland', id: '353' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IL', displayText: 'Israel', id: '972' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IM', displayText: 'Isle of Man', id: '44' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IN', displayText: 'India', id: '91' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IO', displayText: 'British Indian Ocean Territory', id: '246' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IQ', displayText: 'Iraq', id: '964' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IR', displayText: 'Iran, Islamic Republic of', id: '98' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IS', displayText: 'Iceland', id: '354' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'IT', displayText: 'Italy', id: '39' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'JE', displayText: 'Jersey', id: '44' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'JM', displayText: 'Jamaica', id: '1-876' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'JO', displayText: 'Jordan', id: '962' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'JP', displayText: 'Japan', id: '81', suggested: true , flag: require('./images/1.jpg')},
  { code: 'KE', displayText: 'Kenya', id: '254' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KG', displayText: 'Kyrgyzstan', id: '996' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KH', displayText: 'Cambodia', id: '855' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KI', displayText: 'Kiribati', id: '686' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KM', displayText: 'Comoros', id: '269' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KN', displayText: 'Saint Kitts and Nevis', id: '1-869' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KP', displayText: "Korea, Democratic People's Republic of", id: '850' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KR', displayText: 'Korea, Republic of', id: '82' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KW', displayText: 'Kuwait', id: '965' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KY', displayText: 'Cayman Islands', id: '1-345' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'KZ', displayText: 'Kazakhstan', id: '7' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LA', displayText: "Lao People's Democratic Republic", id: '856' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LB', displayText: 'Lebanon', id: '961' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LC', displayText: 'Saint Lucia', id: '1-758' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LI', displayText: 'Liechtenstein', id: '423' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LK', displayText: 'Sri Lanka', id: '94' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LR', displayText: 'Liberia', id: '231' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LS', displayText: 'Lesotho', id: '266' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LT', displayText: 'Lithuania', id: '370' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LU', displayText: 'Luxembourg', id: '352' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LV', displayText: 'Latvia', id: '371' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'LY', displayText: 'Libya', id: '218' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MA', displayText: 'Morocco', id: '212' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MC', displayText: 'Monaco', id: '377' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MD', displayText: 'Moldova, Republic of', id: '373' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ME', displayText: 'Montenegro', id: '382' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MF', displayText: 'Saint Martin (French part)', id: '590' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MG', displayText: 'Madagascar', id: '261' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MH', displayText: 'Marshall Islands', id: '692' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MK', displayText: 'Macedonia, the Former Yugoslav Republic of', id: '389' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ML', displayText: 'Mali', id: '223' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MM', displayText: 'Myanmar', id: '95' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MN', displayText: 'Mongolia', id: '976' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MO', displayText: 'Macao', id: '853' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MP', displayText: 'Northern Mariana Islands', id: '1-670' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MQ', displayText: 'Martinique', id: '596' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MR', displayText: 'Mauritania', id: '222' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MS', displayText: 'Montserrat', id: '1-664' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MT', displayText: 'Malta', id: '356' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MU', displayText: 'Mauritius', id: '230' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MV', displayText: 'Maldives', id: '960' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MW', displayText: 'Malawi', id: '265' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MX', displayText: 'Mexico', id: '52' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MY', displayText: 'Malaysia', id: '60' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'MZ', displayText: 'Mozambique', id: '258' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NA', displayText: 'Namibia', id: '264' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NC', displayText: 'New Caledonia', id: '687' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NE', displayText: 'Niger', id: '227' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NF', displayText: 'Norfolk Island', id: '672' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NG', displayText: 'Nigeria', id: '234' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NI', displayText: 'Nicaragua', id: '505' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NL', displayText: 'Netherlands', id: '31' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NO', displayText: 'Norway', id: '47' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NP', displayText: 'Nepal', id: '977' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NR', displayText: 'Nauru', id: '674' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NU', displayText: 'Niue', id: '683' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'NZ', displayText: 'New Zealand', id: '64' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'OM', displayText: 'Oman', id: '968' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PA', displayText: 'Panama', id: '507' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PE', displayText: 'Peru', id: '51' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PF', displayText: 'French Polynesia', id: '689' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PG', displayText: 'Papua New Guinea', id: '675' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PH', displayText: 'Philippines', id: '63' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PK', displayText: 'Pakistan', id: '92' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PL', displayText: 'Poland', id: '48' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PM', displayText: 'Saint Pierre and Miquelon', id: '508' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PN', displayText: 'Pitcairn', id: '870' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PR', displayText: 'Puerto Rico', id: '1' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PS', displayText: 'Palestine, State of', id: '970' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PT', displayText: 'Portugal', id: '351' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PW', displayText: 'Palau', id: '680' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'PY', displayText: 'Paraguay', id: '595' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'QA', displayText: 'Qatar', id: '974' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'RE', displayText: 'Reunion', id: '262' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'RO', displayText: 'Romania', id: '40' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'RS', displayText: 'Serbia', id: '381' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'RU', displayText: 'Russian Federation', id: '7' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'RW', displayText: 'Rwanda', id: '250' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SA', displayText: 'Saudi Arabia', id: '966' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SB', displayText: 'Solomon Islands', id: '677' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SC', displayText: 'Seychelles', id: '248' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SD', displayText: 'Sudan', id: '249' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SE', displayText: 'Sweden', id: '46' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SG', displayText: 'Singapore', id: '65' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SH', displayText: 'Saint Helena', id: '290' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SI', displayText: 'Slovenia', id: '386' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SJ', displayText: 'Svalbard and Jan Mayen', id: '47' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SK', displayText: 'Slovakia', id: '421' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SL', displayText: 'Sierra Leone', id: '232' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SM', displayText: 'San Marino', id: '378' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SN', displayText: 'Senegal', id: '221' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SO', displayText: 'Somalia', id: '252' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SR', displayText: 'Suriname', id: '597' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SS', displayText: 'South Sudan', id: '211' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ST', displayText: 'Sao Tome and Principe', id: '239' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SV', displayText: 'El Salvador', id: '503' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SX', displayText: 'Sint Maarten (Dutch part)', id: '1-721' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SY', displayText: 'Syrian Arab Republic', id: '963' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'SZ', displayText: 'Swaziland', id: '268' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TC', displayText: 'Turks and Caicos Islands', id: '1-649' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TD', displayText: 'Chad', id: '235' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TF', displayText: 'French Southern Territories', id: '262' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TG', displayText: 'Togo', id: '228' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TH', displayText: 'Thailand', id: '66' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TJ', displayText: 'Tajikistan', id: '992' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TK', displayText: 'Tokelau', id: '690' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TL', displayText: 'Timor-Leste', id: '670' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TM', displayText: 'Turkmenistan', id: '993' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TN', displayText: 'Tunisia', id: '216' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TO', displayText: 'Tonga', id: '676' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TR', displayText: 'Turkey', id: '90' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TT', displayText: 'Trinidad and Tobago', id: '1-868' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TV', displayText: 'Tuvalu', id: '688' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TW', displayText: 'Taiwan, Province of China', id: '886' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'TZ', displayText: 'United Republic of Tanzania', id: '255' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'UA', displayText: 'Ukraine', id: '380' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'UG', displayText: 'Uganda', id: '256' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'US', displayText: 'United States', id: '1', suggested: true , flag: require('./images/1.jpg')},
  { code: 'UY', displayText: 'Uruguay', id: '598' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'UZ', displayText: 'Uzbekistan', id: '998' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'VA', displayText: 'Holy See (Vatican City State)', id: '379' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'VC', displayText: 'Saint Vincent and the Grenadines', id: '1-784' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'VE', displayText: 'Venezuela', id: '58' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'VG', displayText: 'British Virgin Islands', id: '1-284' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'VI', displayText: 'US Virgin Islands', id: '1-340' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'VN', displayText: 'Vietnam', id: '84' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'VU', displayText: 'Vanuatu', id: '678' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'WF', displayText: 'Wallis and Futuna', id: '681' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'WS', displayText: 'Samoa', id: '685' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'XK', displayText: 'Kosovo', id: '383' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'YE', displayText: 'Yemen', id: '967' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'YT', displayText: 'Mayotte', id: '262' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ZA', displayText: 'South Africa', id: '27' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ZM', displayText: 'Zambia', id: '260' , locale: 'Andorra', flag: require('./images/1.jpg')},
  { code: 'ZW', displayText: 'Zimbabwe', id: '263' , locale: 'Andorra', flag: require('./images/1.jpg')},
];
