
import Grid from '@material-ui/core/Grid';
import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import 'date-fns';
import "./style.scss"
export default function DatePicker(props){
  var hideInput = props.hiddenInput ? "input_hidden" : ""
  var onClickT = () =>{
    props.onClick && props.onClick()
  }
    return(
        <MuiPickersUtilsProvider className={hideInput} utils={DateFnsUtils}>
                              <Grid onClick={()=>onClickT()} className={hideInput} container justify="space-around">
                                <KeyboardDatePicker
                                  onChange={(value)=>props.onChange(value)}
                                  disableToolbar
                                  variant="inline"
                                  format={props.format || "dd/MM/yyyy"}
                                  margin="normal"
                                  id="date-picker-inline"
                                  value={new Date('2014-08-18T21:11:54')}
                                  KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                  }}
                                />
                              </Grid>
                            </MuiPickersUtilsProvider>
    )
}