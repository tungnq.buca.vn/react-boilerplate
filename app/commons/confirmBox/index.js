import React, { useState } from 'react'
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import './style.scss'
const ConfrimBox = (props) => {
    const [isOpen, setIsOpen] = useState(props.open)
    const closePopUp = () =>{
        setIsOpen(false)
        props.onClose && props.onClose()
    }
    const handleCallBacl = () =>{
        props.callback && props.callback()
        props.onClose && props.onClose()
    }
    return (
        <Dialog onClose={() =>{
            closePopUp()
        }} open={isOpen}>
            <div className="wrap-confirm-box">
            <div className="title">{props.title}</div>
            <div className="confirm-button">
                <Button className="cancel" variant="contained">
                        <div onClick={() => closePopUp()}>Hủy</div>
                </Button>
                <Button variant="contained" color="primary" className="confirm-ok">
                    <div onClick={() => handleCallBacl() }>OK</div>
                </Button>
            </div>

            </div>
        </Dialog>
    )
}
ConfrimBox.propTypes = {
    open: PropTypes.bool,
    title: PropTypes.string,
    buttonName: PropTypes.string,
    calback: PropTypes.func
};
ConfrimBox.defaultProps = {
    title: 'Đã xảy ra lỗi',
    buttonName: 'OK',
    open: true,
    callback: () => { console.log('callback') }
}

export default ConfrimBox