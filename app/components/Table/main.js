import React, { useEffect, memo, useState, useRef, Fragment } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { Pagination, PaginationItem } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';


import DatePicker from 'commons/datepicker'
import Countrybox from 'commons/countriesbox'
import ComboBox from 'commons/combobox'
import PopupTrigger from "commons/popuptrigger";
import ConfirmBox from "commons/confirmBox2";
import TranformHepper from "commons/transform-helper.js"
import { connect } from 'react-redux';
import { compose } from 'redux';
import { emailRegex } from 'utils/regex'
import {
  fetchDataTable, changeOrderSort, deleteRow, changePage,
  changeFilter, checkMultiDelete, deleteMulti,
  chageUpdateColumn, setActiveRow, updateColumn, setInitial, fetAllDataWithLimited
} from './actions'
import { convertDate } from "utils/convertDate"
import { makeHomeState } from './selectors'
import EditTable from './edit'
import Input from './components/input'
import './index.scss'


const useStyles = makeStyles(theme => ({
  container: {
    maxHeight: 440,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
const renderInputSearch = (column, onchangeFilter) => {
  if (column.isDateFormat) return (<DatePicker onChange={(value) => {
    var filter = column
    onchangeFilter(value, filter)
  }} />)
  if (column.isComboBox) return (
    <ComboBox onChange={(value) => {
      var filter = column
      onchangeFilter(value, filter)
    }} />
  )
  return (
    <input className="filter-data" data={column.id} onChange={(e) => onchangeFilter(e.target.value, column)} />
  )

}

var searchTimer2
var timmerEditInline
export function TableBuCa({
  onFetchDataTable,
  onChangePage,
  onChangeOrDerSort,
  onSetInitial,
  onCheckMultiDelete,
  onDeleteMuntil,
  onchageUpdateColumn,
  onSetActiveRow,
  onUpdateColumn,
  onFetAllDataWithLimited,
  onchangeFilter,
  className, higherProps, onDeleteRow, homeState }) {

  const refBody = useRef()
  const refOption = useRef()
  const { randomNumber } = homeState
  const _options = higherProps.options
  const limited = _options.limit || 1000
  const _URL = _options.url
  const urlPost = _options.urlPost
  const pageSize = _options.rowPerPage || 20
  var _columns = _options.columns


  const lengthAlldata = homeState.lengthAlldata
  const data = homeState.data
  const commonColumn = homeState.commonColumn
  const page = homeState.page
  const order = homeState.order
  const filter = homeState.filter
  const classes = useStyles();
  const [getTime, setGetTime] = useState(0)
  const reRender = () => {
    setGetTime(new Date().getTime())
  }
  const togleOption = (_class) => {
    // remove class open của element trước
    var elementOpen = refBody.current.getElementsByClassName('open-option')
    elementOpen && elementOpen.length > 0 && elementOpen[0] && elementOpen[0].classList.remove('open-option')
    // add class open cho element được click
    var element = refBody.current.getElementsByClassName(_class)
    var el_current = element && element.length > 0 && element[0].classList.toggle('open-option')
  }
  const handleClickDocument = (e) => {
    //remove class open hiện tại
    var elementFinder = refBody.current.getElementsByClassName('open-option')
    var ele = elementFinder && elementFinder.length > 0 && elementFinder[0]
    if (ele && !ele.contains(e.target)) {
      ele.classList.remove('open-option')
    }
  }
  // const clickToEdit = (event, row)=>{
  //   event.preventDefault();
  //   event.stopPropagation();
  //   event.target.children && event.target.children.length > 0 && event.target.children[0].classList.add('active')
  //   event.target.children && event.target.children.length > 0 && event.target.children[0].focus()
  //   onSetActiveRow(row)
  // }


  const handdleOutSide = (value, column) => {
    if (column.isEmail) {
      const validateEmail = new RegExp(emailRegex, 'g').test(value)
      if (!validateEmail) {
        IMSPlugins.TransformHelper.showPopup(ConfirmBox, {
          open: true,
          title: "Email không hợp lệ",
          onClose: () => {
            debugger
            IMSPlugins.TransformHelper.closePopup(ConfirmBox)
          }
        })

      } else {
        onUpdateColumn()
      }
    } else {
      onUpdateColumn()
    }


  }
  useEffect(() => {
    onSetInitial(_URL, limited, pageSize)
    onFetAllDataWithLimited()
  }, [homeState.lengthAlldata])
  useEffect(() => {
    // let urlFetch = _URL + '?_page=' + page + '&_limit=' + pageSize
    // for (var property in filter) {
    //   if (filter[property]) {
    //     urlFetch = urlFetch + '&' + property + '=' + filter[property]
    //   }
    // }
    // var allSort = []
    // var allOrder = []
    // var _strSort = ''
    // for (var property in order) {
    //   if (order[property]) {
    //     allSort.push(property)
    //     allOrder.push(order[property])
    //     _strSort = '&_sort=' + allSort.join(',') + '&_order=' + allOrder.join(',')
    //   }
    // }
    // urlFetch += _strSort
    if (searchTimer2) {
      clearTimeout(searchTimer2);
    }
    searchTimer2 = setTimeout(function () {
      onFetchDataTable()
    }, 400)
    // document.addEventListener("click", handleClickDocument);

  }, [getTime, randomNumber]);
  const countPage = Math.ceil(lengthAlldata / pageSize)
  return (
    <div className="Buca-wrapper-table">
      <button onClick={() => onDeleteMuntil()}>delete select</button>
      <PopupTrigger
        renderPopup={({ handleOpen, handleClose }) => <EditTable columnFomart={_columns} column={commonColumn} isAdd={true} callback={() => {
          handleClose()
          reRender()
        }}
          url={urlPost} />}>
        {
          ({ handleOpen }) => (
            <li onClick={handleOpen} className="edit">
              Thêm
            </li>
          )
        }
      </PopupTrigger>


      <Paper className="content-table">
        <TableContainer className={classes.container + ""}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {_columns.map((column, index) => {
                  var classname_order = ""

                  return (
                    <TableCell
                      key={index}
                      align={column.align}
                      style={{ minWidth: column.minWidth }}
                    >
                      {!column.actionOption && !column.multiDelete &&
                        <div>
                          <div className={classname_order + " title-table"} onClick={(e) => { onChangeOrDerSort(order, column.id) }}>{column.label} <span></span></div>
                          <div className="fillter-input">
                            {
                              renderInputSearch(column, onchangeFilter)
                            }

                          </div>
                        </div>

                      }
                    </TableCell>
                  )
                })}
              </TableRow>
            </TableHead>
            <TableBody ref={refBody} >
              {data && data.length > 0 && data.map((row, stt) => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={stt}>
                    {_columns.map((column, index) => {
                      const valueCol = row[column.id];
                      const label = column.format && typeof valueCol === 'number' ? valueCol.toLocaleString() : valueCol
                      return (
                        <Fragment key={index}>
                          {!column.multiDelete && !column.actionOption &&
                            <TableCell key={index} >

                              {!column.isComboBox &&
                                <Fragment>
                                  <Input onChange={(e) => onchageUpdateColumn(e.target.value, column.id)}
                                    onTrigger={() => onSetActiveRow(row)}
                                    clickOutSide={(value) => {
                                      var col = column
                                      handdleOutSide(value, col)
                                    }}
                                    className={"input-columns-edit-inline"}
                                    defaultValue={label}
                                  >
                                  </Input>
                                  {label}
                                </Fragment>
                              }
                              {column.isDateFormat &&
                                <DatePicker onClick={() => {onSetActiveRow(row) }}
                                  onChange={(value) => {
                                    onchageUpdateColumn(convertDate(value), column = column.id)
                                    setTimeout(() => {
                                      onUpdateColumn()
                                    }, 500);
                                  }}
                                  hiddenInput={true} />
                              }
                              {
                                column.isComboBox &&
                                <ComboBox
                                defaulValue = {valueCol}
                                 onClick={() => { onSetActiveRow(row) }}
                                  onChange={(value) => {
                                    onchageUpdateColumn(value, column = column.id)
                                    setTimeout(() => {
                                      onUpdateColumn()
                                    }, 500);
                                  }} />
                              }

                            </TableCell>
                          }
                          {
                            column.multiDelete &&
                            <TableCell key={index}>
                              <input onClick={(e) => onCheckMultiDelete(e)} value={row.ACCOUNT_ID || row.id} type="checkbox"></input>
                            </TableCell>
                          }
                          {
                            column.actionOption &&
                            <TableCell key={index}>
                              <div onClick={(e) => togleOption("option-table-" + stt)} className={"option-table " + "option-table-" + stt}>
                                <span className="click-tick"></span>
                                <ul ref={refOption}>
                                  {
                                    column.isEdit &&
                                    <PopupTrigger
                                      renderPopup={({ handleOpen, handleClose }) => <EditTable
                                        columnFomart = {_columns}
                                        id={row.id}
                                        url={_URL} 
                                        isEdit={true}
                                        callback={() => {
                                          handleClose()
                                          reRender()
                                      }}/>}>
                                      {
                                        ({ handleOpen }) => (
                                          <li onClick={handleOpen} className="edit">
                                            <span >
                                              <svg className="action-option" viewBox="0 0 14 14" xmlns="http://www.w3.org/2000/svg" id="icon-edit">
                                                <path d="M8.33333 2.33333L11.6667 5.66666" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path>
                                                <path d="M4.68228 12.8932C4.78507 12.8761 4.87994 12.8272 4.95363 12.7535L13.0022 4.70497L13.0023 4.70492C13.321 4.38611 13.5 3.95378 13.5 3.503C13.5 3.05221 13.321 2.61988 13.0023 2.30108L13.0022 2.30103L11.699 0.997798L11.6989 0.997744C11.3801 0.679039 10.9478 0.5 10.497 0.5C10.0462 0.5 9.61389 0.679039 9.29508 0.997744L9.29503 0.997798L1.24646 9.04637C1.17277 9.12006 1.12395 9.21493 1.10682 9.31772L0.506803 12.9178C0.480257 13.0771 0.532266 13.2394 0.646447 13.3536C0.760627 13.4677 0.922921 13.5197 1.0822 13.4932L4.68228 12.8932Z" strokeMiterlimit="10" strokeLinecap="round" strokeLinejoin="round"></path>
                                              </svg>
                                            </span>
                                            Sửa
                                          </li>
                                        )
                                      }
                                    </PopupTrigger>
                                  }
                                  {
                                    column.isDelete &&
                                    <PopupTrigger
                                      renderPopup={({ handleOpen, handleClose }) => <ConfirmBox callback={() => onDeleteRow(row.id, _URL)} onClose={handleClose} open={true} title="Bạn có muốn xóa không?" />}>
                                      {
                                        ({ handleOpen }) => (
                                          <li onClick={handleOpen} className="delete"><span >
                                            <svg className="action-option" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" id="icon-delete">
                                              <path fillRule="evenodd" clipRule="evenodd" d="M1 4.5C1 4.22386 1.3134 4 1.7 4H14.3C14.6866 4 15 4.22386 15 4.5C15 4.77614 14.6866 5 14.3 5H1.7C1.3134 5 1 4.77614 1 4.5Z"></path>
                                              <path fillRule="evenodd" clipRule="evenodd" d="M6.16667 2C5.98986 2 5.82029 2.07023 5.69526 2.19526C5.57024 2.32028 5.5 2.48985 5.5 2.66666V4.00004H10.5V2.66666C10.5 2.48985 10.4298 2.32028 10.3047 2.19526C10.1797 2.07023 10.0101 2 9.83333 2H6.16667ZM11.5 4.00004V2.66666C11.5 2.13623 11.3751 1.71331 11 1.33824C10.625 0.963164 10.3638 1.00002 9.83333 1.00002H6.16667C5.63623 1.00002 5.4609 0.963141 5.08583 1.33821C4.71076 1.71329 4.50004 2.13623 4.50004 2.66666V4.00004H3.66667C3.29848 4.00004 3 4.63181 3 5V13.3333C3 13.5 3.12493 14.125 3.5 14.5C3.87507 14.8751 4.46957 15 5 15H11.3333C11.8637 15 12.1249 14.8751 12.5 14.5C12.8751 14.125 13 13.8638 13 13.3333V5C13 4.63181 12.7015 4.00004 12.3333 4.00004H11.5ZM3.99996 5.00004V13.3333C3.99996 13.5101 4.0702 13.6797 4.19522 13.8047C4.32025 13.9298 4.48981 14 4.66663 14H11.3333C11.5101 14 11.6797 13.9298 11.8047 13.8047C11.9297 13.6797 12 13.5101 12 13.3333V5.00004H3.99996Z"></path>
                                            </svg>
                                          </span>
                                            Xóa
                                          </li>
                                        )
                                      }
                                    </PopupTrigger>
                                  }
                                </ul>
                              </div>

                            </TableCell>
                          }
                        </Fragment>

                      );
                    })}

                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
        {
          higherProps.options.isPagination &&
          <Pagination onChange={onChangePage} count={countPage} className={className + ' BuCa-MuiPagination'} />
        }
      </Paper>
    </div>
  );
}

TableBuCa.prototype = {
  className: PropTypes.string
}
TableBuCa.defaultProp = {
  isPagination: true,
  className: ''
}
const mapStateSelectToProps = createStructuredSelector({
  homeState: makeHomeState()
});

export function mapDispatchToProps(dispatch, higherProps) {
  return {
    onFetchDataTable: (url, urlFetch, limitRow, pageSize) => {
      dispatch(fetchDataTable(url, urlFetch, limitRow, pageSize))
    },
    onDeleteMuntil: () => {
      dispatch(deleteMulti())
    },
    onChangePage: (event, newPage) => {
      dispatch(changePage(newPage))
    },
    onCheckMultiDelete: (e) => {
      dispatch(checkMultiDelete(e.target.value))
    },
    onChangeOrDerSort: (order, dataType) => {
      var field = order.filter(item => {
        return item.fieldName == dataType
      })
      if (!field || field.length == 0) {
        order.push({
          fieldName: dataType,
          typeShort: 'asc'
        })
      }
      order.forEach(item => {
        if (item.fieldName == dataType) {
          if (item.typeShort == 'asc') {
            item.typeShort = 'desc'
          } else if (item.typeShort == 'desc') {
            item.typeShort = ''
          } else {
            item.typeShort = 'asc'
          }
        }
      });
      dispatch(changeOrderSort(order))
    },
    onchangeFilter: (value, filter) => {
      filter[filter.id] = value
      dispatch(changeFilter(filter))
    },
    onchageUpdateColumn: (value, dataType) => {
      if (timmerEditInline) {
        clearTimeout(timmerEditInline);
      }
      timmerEditInline = setTimeout(function () {
        dispatch(chageUpdateColumn(value, dataType))
      }, 400)

    },
    onSetActiveRow: (row) => {
      dispatch(setActiveRow(row))
    },
    onUpdateColumn: () => {
      dispatch(updateColumn())
    },
    onDeleteRow: (id, url) => {
      dispatch(deleteRow(id, url))
    },

    onSetInitial: (_URL, limited, pageSize) => {
      dispatch(setInitial(_URL, limited, pageSize))
    },
    onFetAllDataWithLimited: () => {
      dispatch(fetAllDataWithLimited())
    },

    higherProps: { ...higherProps }
  }
}

const withConnect = connect(
  mapStateSelectToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TableBuCa);