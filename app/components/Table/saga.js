
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ON_DELETE, FETCH_DATA_TABLE, DELETE_MULTI, ON_UPDATE_COLUMN, ON_FETCH_ALL_DATA_LIMITED  } from './constants'
import { refeshComp, updateStore, updateCountPage } from './actions'
import { makeHomeState } from './selectors'
import request from 'utils/request';
import { update } from 'offline-plugin/runtime';

/**
 * Root saga manages watcher lifecycle
 */
export function* onDelete() {
  const homeState = yield select(makeHomeState())
  const idDelete = homeState.idDelete
  const url = homeState.url
  const requestURL = url + '/' + idDelete;
  let requestOptions = {
    method: 'DELETE',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': true
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
  };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, requestOptions);
    yield put(refeshComp());
  } catch (err) {
    console.log('err', err)
  }
}
export function* fetchData() {
  const homeState = yield select(makeHomeState())
  const pageSize = homeState.pageSize
  const url = homeState.url
  const page = homeState.page
  const order = homeState.order
  const filter = homeState.filter
  let getallDataOnePage = {
    method: 'POST',
    cache: 'no-cache', 
    credentials: 'same-origin',
    headers: { 
                  'Content-Type': 'application/json',
      },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify({
      url,
      limitRow: pageSize,
      page,
      pageSize,
      order,
      filter
    })
};
  try {
    const data = yield call(request, url, getallDataOnePage);
    yield put(updateStore(data));
  } catch (err) {
    console.log('err', err)
  }

}
export function* fetAllDataWithLimited() {
  const homeState = yield select(makeHomeState())
  const url = homeState.url
  const limitRow = homeState.limitRow
  let getallData = {
    method: 'POST',
    cache: 'no-cache', 
    credentials: 'same-origin',
    headers: { 
                  'Content-Type': 'application/json',
      },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify({
      url,
      limitRow,
      page: 1
    })
};
  try {
    const allData = yield call(request, url, getallData);
    yield put(updateCountPage(allData.length));
  } catch (err) {
    console.log('err', err)
  }

}
export function* deleteMulti() {
  const homeState = yield select(makeHomeState())
  const ids_delete = homeState.ids_delete
  const ids = ids_delete.join(',')
  const url = homeState.url + '/' + ids
  var requestURL = url
  let requestOptions = {
    method: 'DELETE',
  };
  try {
    const result = yield call(request, requestURL, requestOptions);
    yield put(refeshComp())
  } catch (err) {
    console.log('err', err)
  }

}
export function* updateColumn() {
  const homeState = yield select(makeHomeState())
  var rowActive = homeState.rowActive
  console.log('rowActive', rowActive)
  const url = homeState.url + '/' + rowActive.id
  let requestOptions = {
    method: 'PUT',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
    },
    redirect: 'follow',
    referrerPolicy: 'no-referrer',
    body: JSON.stringify( rowActive )
  };

  try {
    const result = yield call(request, url, requestOptions);
    yield put(refeshComp())
  } catch (err) {
    console.log('err', err)
  }
}
export default function* WatchesActions() {
  yield takeLatest(ON_DELETE, onDelete);
  yield takeLatest(FETCH_DATA_TABLE, fetchData);
  yield takeLatest(DELETE_MULTI, deleteMulti)
  yield takeLatest(ON_UPDATE_COLUMN, updateColumn)
  yield takeLatest(ON_FETCH_ALL_DATA_LIMITED, fetAllDataWithLimited)
}
