import React from 'react';
//Redux
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import Main from './main'
import  reducers  from './reducer';
//Redux saga
import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga'; 

const sagaMiddleware = createSagaMiddleware();
let store = createStore(reducers, composeWithDevTools(applyMiddleware(sagaMiddleware)));
// import store from './store'

const App = (props) => {
    const _options = props.options
    var columns = props.options.columns
    if(_options.isEdit || _options.isDelete ){
        var moreOpts = {
            actionOption: true,
            isEdit:  _options.isEdit,
            isDelete: _options.isDelete
          }
          columns.push(moreOpts)
      } 
      if(_options.multiDelete){
        columns.unshift({multiDelete: true})
      }
      
    return(
        <Provider store={store}>
            <Main {...props}/>
        </Provider>
    )
}
sagaMiddleware.run(rootSaga);
export default App;