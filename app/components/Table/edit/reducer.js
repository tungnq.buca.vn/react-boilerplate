/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import { FETCH_DATA_TABLE, UPDATE_DATA, ON_CHANGE_FALG} from './constants';
import { combineReducers } from "redux";

// The initial state of the App
export const initialState = {
  pageIndex: 1,
  data: [],
  flagSort: 0,
  // columns : [
  //   { id: 'name', label: 'Name', minWidth: 170 },
  //   { id: 'code', label: 'ISO\u00a0Code', minWidth: 100 },
  //   {
  //     id: 'population',
  //     label: 'Population',
  //     minWidth: 170,
  //     align: 'right',
  //     format: value => value.toLocaleString(),
  //   },
  //   {
  //     id: 'size',
  //     label: 'Size\u00a0(km\u00b2)',
  //     minWidth: 170,
  //     align: 'right',
  //     format: value => value.toLocaleString(),
  //   },
  //   {
  //     id: 'density',
  //     label: 'Density',
  //     minWidth: 170,
  //     align: 'right',
  //     format: value => value.toFixed(2),
  //   },
  // ],
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>{
  switch (action.type) {
    case FETCH_DATA_TABLE:
      return{
        ...state,
        pageIndex: action.pageIndex

      }
    case UPDATE_DATA:
      return{
        ...state,
        data: action.data

      }
    case ON_CHANGE_FALG:
      let flag
      if (action.flag == 0){
        flag = 1
      }else if(action.flag == 1){
        flag = 2
      }else{
        flag = 0
      }
      return{
        ...state,
        flagSort: flag

      }
    default:
      return {
        ...state
      }
  }
}





  export default combineReducers({
    homeState: homeReducer
  });

  
// export default homeReducer;
