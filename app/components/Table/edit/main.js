import React, { useEffect, memo, useState, Fragment } from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { compose } from 'redux';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import DatePicker from 'commons/datepicker'
import ComboBox from 'commons/combobox'
import { convertDate } from "utils/convertDate"
import { makeHomeState } from './selectors'
import './index.scss'

const convertColumn = (data) => {
  let column = {}
  if (!data || !data.length) { return column }
  let temp = data[0]
  for (var key in temp) {
    if (temp.hasOwnProperty(key)) {
      column[key] = key
    }
  }
  return column
}

export function TableBuCa({ url, id, callback, column, columnFomart, isAdd }) {
  var columnReformat = Object.assign({}, column)
  if (columnReformat.id || columnReformat.Id) {
    delete columnReformat.id
    delete columnReformat.Id
  }
  const [data, setData] = useState({})
  const [getTime, setGetTime] = useState(0)
  const [columnTable, setColumn] = useState(columnReformat || {})
  // const [formData, setFormData] = useState({})
  const setBodyFormData = (value, typeData) => {
    data[typeData] = value
    setData(data)
    setGetTime(new Date().getTime())
  }
  const onChangeInput = (e, typeData)=>{
    var value = e.target.value
    setBodyFormData(value, typeData)
  }
  var filedDateFormat = []
  var filedEmailFormat = []
  var filedComboBoxFormat = []
  columnFomart.filter(el => {
    if (el.isDateFormat) {
      filedDateFormat.push(el.id)
    } else if (el.isEmail) {
      filedEmailFormat.push(el.id)
    } else if (el.isComboBox) {
      filedComboBoxFormat.push(el.id)
    }
  })

  const finishUpdateData = () => {
    const _url = id ? url + '/' + id : url
    var method = id ? 'PUT' : 'POST'
    fetch(_url, {
      method: method,
      // mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
        // 'Content-Type': 'application/x-www-form-urlencoded',
      },
      redirect: 'follow',
      referrerPolicy: 'no-referrer',
      body: JSON.stringify(data)
    }).then(res => {
      if (!res || !res.status) return
      callback && callback()
    })
      .catch(error => {
        console.log('error', error)
      })
  }
  useEffect(() => {
    if (!isAdd) {
      fetch(url + '/' + id)
        .then(function (res) {
          return res.text()
        })
        .then(function (result) {
          if (!result || !result.length) return
          // const column = convertColumn(JSON.parse(result))
          delete result.id
          setData(JSON.parse(result)) // chỉ có 1 kết quả trả về
          // setFormData(JSON.parse(result))
          setColumn(JSON.parse(result))
          setGetTime(new Date().getTime())
        })
        .catch(function (error) {
          console.log('error', error)
        });
    }
  }, getTime, data);
  if (typeof columnTable.id !== 'undefined') {
    delete columnTable.id
  }
  if (typeof data.id !== 'undefined') {
    delete data.id
  }
  console.log('data', data)

  return (
    <div className="Buca-wrapper-table-edit">
      <TableContainer>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {
                Object.keys(columnTable).map((key, index) => {
                  return (
                    <TableCell key={index}>
                      {key}
                    </TableCell>
                  )
                })
              }
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              {
                !isAdd ?
                  Object.keys(data).map((key, index) => {
                    switch (true) {
                      case filedDateFormat.indexOf(key) >= 0:
                        return (
                          <TableCell key={index}>
                            <input onChange={(e) => onChangeInput(e, key)} className="edit-field" value={data[key]}></input>
                            <DatePicker 
                              hiddenInput={true}
                              onChange={(value) => {

                                setBodyFormData(convertDate(value), key = key)
                              }}
                               />
                          </TableCell>
                        )
                      case filedEmailFormat.indexOf(key) >= 0:
                        return (
                          <TableCell key={index}>
                            <input onChange={(e) => onChangeInput(e, key)} className="edit-field" value={data[key]}></input>
                          </TableCell>
                        )
                      case filedComboBoxFormat.indexOf(key) >= 0:
                        return (
                          <TableCell key={index}>
                            <ComboBox 
                            defaulValue={data[key]}
                            onChange={(value) => {
                              setBodyFormData(value, key = key)
                            }} />
                          </TableCell>
                        )
                      default:
                        return (
                          <TableCell key={index}>
                            <input onChange={(e) => onChangeInput(e, key)} className="edit-field" value={data[key]}></input>
                          </TableCell>
                        )
                    }
                  })
                  :
                  Object.keys(columnTable).map((key, index) => {
                    switch (true) {
                      case filedDateFormat.indexOf(key) >= 0:
                        return (
                          <TableCell key={index}>
                            <input onChange={(e) => onChangeInput(e, key)} className="edit-field" value={data[key]}></input>
                            <DatePicker 
                              hiddenInput={true}
                              onChange={(value) => {

                                setBodyFormData(convertDate(value), key = key)
                              }}
                               />
                          </TableCell>
                        )
                      case filedEmailFormat.indexOf(key) >= 0:
                        return (
                          <TableCell key={index}>
                            <input onChange={(e) => onChangeInput(e, key)} className="edit-field" value={data[key]}></input>
                          </TableCell>
                        )
                      case filedComboBoxFormat.indexOf(key) >= 0:
                        return (
                          <TableCell key={index}>
                            <ComboBox 
                            defaulValue={data[key]}
                            onChange={(value) => {
                              setBodyFormData(value, key = key)
                            }} />
                          </TableCell>
                        )
                      default:
                        return (
                          <TableCell key={index}>
                          <input onChange={(e) => onChangeInput(e, key)} className="edit-field" value={data[key]}></input>
                        </TableCell>
                        )
                    }

                  })
              }
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
      <Button onClick={() => finishUpdateData()} className="confirm-btn" variant="contained" color="primary">ok</Button>
    </div>
  );
}

TableBuCa.prototype = {
  className: PropTypes.string
}
TableBuCa.defaultProp = {
  isPagination: true,
  className: ''
}
const mapStateSelectToProps = createStructuredSelector({
  state: makeHomeState()
});

export function mapDispatchToProps(dispatch, higherProps) {
  return {
    higherProps: { ...higherProps }
  }

}

const withConnect = connect(
  mapStateSelectToProps,
  mapDispatchToProps,
);

export default compose(
  withConnect,
  memo,
)(TableBuCa);