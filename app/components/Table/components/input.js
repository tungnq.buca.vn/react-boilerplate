// import React, { useEffect, useRef, useState } from "react";

// const Input = (props) => {
//     var refInputInline = useRef()

//     const handleClickInside = (e) => {
//         props.clickInside && props.clickInside(e)
//     }
//     const handleClickOutside = (e) => {
//         var value = refInputInline.current.value
//         props.clickOutSide && props.clickOutSide(value)
//     }
//     const handOnChange = (e) => {
//         props.onChange && props.onChange(e)
//     }
//     useEffect(() => {
//         document.addEventListener("mousedown", handleClickOutside);
//     });

//     return (
//         <input ref={refInputInline} onChange={(e) => handOnChange(e)} className={props.className || ''} onClick={() => handleClickInside()} defaultValue={props.defaultValue || ''}></input>
//     );
// };

// export default Input;


import React, { useState, useEffect, useRef } from 'react';

// Usage
export default (props) => {
  // Create a ref that we add to the element for which we want to detect outside clicks
  const ref = useRef();
  var refInputInline = useRef()
  // State for our modal
  const [isModalOpen, setModalOpen] = useState(false);
  // Call hook passing in the ref and a function to call on outside click
  useOnClickOutside(ref, () =>{
    var value = refInputInline.current.value
    props.clickOutSide && props.clickOutSide(value)
    setModalOpen(false)
  })
  return (
    <div className="wrapper-input">
      {isModalOpen ? (
        <div ref={ref}>
          <input ref={refInputInline} onChange={(e) => props.onChange(e)} className={props.className || ''} defaultValue={props.defaultValue || ''}></input>
        </div>
      ) : (
      <div className="div-trigger" onClick={() => {
        setModalOpen(true)
        props.onTrigger && props.onTrigger()
      }}>{props.children}</div>
      )}
    </div>
  );
}

// Hook
function useOnClickOutside(ref, handler) {
  useEffect(
    () => {
      const listener = event => {
        // Do nothing if clicking ref's element or descendent elements
        if (!ref.current || ref.current.contains(event.target)) {
          return;
        }

        handler(event);
      };

      document.addEventListener('mousedown', listener);
      document.addEventListener('touchstart', listener);

      return () => {
        document.removeEventListener('mousedown', listener);
        document.removeEventListener('touchstart', listener);
      };
    },
    // Add ref and handler to effect dependencies
    // It's worth noting that because passed in handler is a new ...
    // ... function on every render that will cause this effect ...
    // ... callback/cleanup to run every render. It's not a big deal ...
    // ... but to optimize you can wrap handler in useCallback before ...
    // ... passing it into this hook.
    [ref, handler]
  );
}