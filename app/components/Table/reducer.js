/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import { FETCH_DATA_TABLE, UPDATE_DATA, ON_CHANGE_ORDER, ON_DELETE, REFESH_COMP,
  UPDATE_STORE, CHANGE_PAGE, ON_CHANGE_FILTER, CHECK_MULTI_DELETE,
  CHANGE_ROW_ACTIVE, SET_ACTIVE_ROW, ON_SET_INITIAL, SET_COUNT_ROW, ON_FETCH_ALL_DATA_LIMITED   } from './constants';
import { combineReducers } from "redux";

// The initial state of the App
export const initialState = {
  lengthAlldata: 0,
  commonColumn: 0,
  limitRow: 100,
  pageSize: 0,
  data: [],
  ids_delete: [],
  page: 1,
  activeInputEdit: null,
  rowActive: null,
  // order: {fullName: 'asc', shortName: 'abc'},
  order: [
  ],
  filter: {},
  idDelete: null,
  url: '',
  urlFetch: '',
  randomNumber: null
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ON_SET_INITIAL:
      return {
        ...state,
        limitRow: action.limitRow,
        url: action.url,
        pageSize: action.pageSize
      }
    case UPDATE_DATA:
      return {
        ...state,
        data: action.data
      }
    case SET_COUNT_ROW:
      return {
        ...state,
        lengthAlldata: action.lengthAlldata

      }
    case ON_CHANGE_ORDER:
      return {
        ...state,
        order: action.order,
        randomNumber: new Date().getTime()

      }
    case ON_CHANGE_FILTER:
      return {
        ...state,
        filter: action.filter,
        randomNumber: new Date().getTime()

      }
    case ON_DELETE:
      return {
        ...state,
        idDelete: action.idDelete,
        url: action.url
      }
    case REFESH_COMP:
      return {
        ...state,
        randomNumber: new Date().getTime()
      }
    case UPDATE_STORE:
      var commonColumn = {}
      Object.keys(action.data[0]).map((key, index) => {
        commonColumn[key]=key
      })
      return {
        ...state,
        data: action.data,
        commonColumn: commonColumn,
      }
    case CHANGE_PAGE:
      return {
        ...state,
        page: action.page,
        randomNumber: new Date().getTime()
      }
    case CHECK_MULTI_DELETE:
      var ids_delete = state.ids_delete
      var result = []
      if (ids_delete.indexOf(action.id) >= 0) {
        result = ids_delete.filter(el => {
          return el != action.id
        })
      } else {
        ids_delete.push(action.id)
      }
      return {
        ...state,
        ids_delete

      }
      case CHANGE_ROW_ACTIVE:
        var rowActive = state.rowActive
        rowActive[action.dataType] = action.value
        return{
          ...state,
          rowActive: rowActive,

        }
      case SET_ACTIVE_ROW:
        return{
          ...state,
          rowActive: action.rowActive,

        }
      case ON_FETCH_ALL_DATA_LIMITED:
        return{
          ...state
        }
    default:
      return {
        ...state
      }
  }
}





export default combineReducers({
  homeState: homeReducer
});


// export default homeReducer;
