/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const FETCH_DATA_TABLE = 'FETCH_DATA_TABLE';
export const ON_SET_INITIAL = 'ON_SET_INITIAL';
export const SET_COUNT_ROW = 'SET_COUNT_ROW';
export const UPDATE_DATA = 'UPDATE_DATA';
export const ON_CHANGE_ORDER = 'ON_CHANGE_ORDER';
export const ON_CHANGE_FILTER = 'ON_CHANGE_FILTER';
export const ON_DELETE = 'ON_DELETE';
export const REFESH_COMP = 'REFESH_COMP';
export const UPDATE_STORE = 'UPDATE_STORE';
export const CHANGE_PAGE = 'CHANGE_PAGE';
export const CHECK_MULTI_DELETE = "CHECK_MULTI_DELETE"
export const DELETE_MULTI = "DELETE_MULTI"
export const CHANGE_ROW_ACTIVE = "CHANGE_ROW_ACTIVE"
export const SET_ACTIVE_ROW  = "SET_ACTIVE_ROW"
export const ON_UPDATE_COLUMN  = "ON_UPDATE_COLUMN"
export const ON_FETCH_ALL_DATA_LIMITED  = "ON_FETCH_ALL_DATA_LIMITED"
