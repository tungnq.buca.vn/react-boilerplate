/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { FETCH_DATA_TABLE, ON_CHANGE_ORDER , ON_DELETE, REFESH_COMP, UPDATE_STORE,
  CHANGE_PAGE, ON_CHANGE_FILTER, CHECK_MULTI_DELETE, DELETE_MULTI, CHANGE_ROW_ACTIVE,
  SET_ACTIVE_ROW, ON_UPDATE_COLUMN, ON_SET_INITIAL, ON_FETCH_ALL_DATA_LIMITED, SET_COUNT_ROW } from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {string} email The new text of the input field
 *
 * @return {object} An action object with a type of ONSUBMIT
 */


export function fetchDataTable() {
  return {
    type: FETCH_DATA_TABLE,
  };
}
export function fetAllDataWithLimited() {
  return {
    type: ON_FETCH_ALL_DATA_LIMITED,
  };
}
export function setInitial(url, limitRow, pageSize) {
  return {
    type: ON_SET_INITIAL,
    url,
    limitRow,
    pageSize
  };
}
export function updateData(data) {
  return {
    type: UPDATE_DATA,
    data
  };
}
export function changeOrderSort(order) {
  return {
    type: ON_CHANGE_ORDER,
    order
  };
}
export function deleteRow(id, url) {
  return {
    type: ON_DELETE,
    idDelete: id,
    url
  };
}
export function refeshComp() {
  return {
    type: REFESH_COMP
  };
}
export function updateStore(data) {
  return {
    type: UPDATE_STORE,
    data,
  };
}
export function updateCountPage( lengthAlldata) {
  return {
    type: SET_COUNT_ROW,
    lengthAlldata
  };
}
export function changePage(page) {
  return {
    type: CHANGE_PAGE,
    page
  };
}
export function changeFilter(filter) {
  return {
    type: ON_CHANGE_FILTER,
    filter
  };
}
export function checkMultiDelete(id){
  return{
    type: CHECK_MULTI_DELETE,
    id
  }
}
export function deleteMulti(){
  return{
    type: DELETE_MULTI
  }
}
export function setActiveRow(rowActive){
  return{
    type: SET_ACTIVE_ROW,
    rowActive
  }
}
export function chageUpdateColumn(value, dataType){
  return{
    type: CHANGE_ROW_ACTIVE,
    dataType,
    value
  }
}
export function updateColumn(){
  return{
    type: ON_UPDATE_COLUMN,
  }
}
