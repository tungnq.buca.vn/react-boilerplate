import React, {useEffect, memo, useState, useRef, Fragment} from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import Sortable from 'sortablejs';
import {handleUpload, deleteImage} from './actions'
import {makeSelectFiles, makeHomeState} from './selectors'
import './index.scss'


  export function UpLoadForm({onHandleUpload, onDeleteImage, state}) {
    const refInput = useRef()
    const refWrapImage = useRef()
    const refWrapInput = useRef()
    const {images} = state
    const dragOverCb = ()=>{
      refWrapInput.current.classList.add("drag-over")
    }
    const ondragleaveCb = ()=>{
      refWrapInput.current.classList.remove("drag-over")
    }
    useEffect(() => {
      refInput.current.addEventListener("dragover", dragOverCb);
      refInput.current.addEventListener("dragleave", ondragleaveCb);
      // refInput.addEventListener("drop", dropCb);




      var sortable = new Sortable(refWrapImage.current, {
        animation: 150,
        ghostClass: 'blue-background-class'
      })
    })
    return (
      <div className="Buca-wrapper-uploadfile">
        
        <div ref={refWrapInput} className="suggest-upload-panel">
          <div className="button-upload">
            <i className="_icon icon-upload-cloud"></i>
            Upload ảnh từ máy tính
            <input multiple={true} onChange={(e)=>onHandleUpload(e.target.files)} ref = {refInput} accept="image/png, image/jpeg" title="Chọn file" type="file"></input>
          </div>
          <div className="upload-by-url">
          <i className="_icon icon-upload-url"></i>
            Upload ảnh bằng URL
          </div>
          <div ref={refWrapImage} className="all-image">
            {
              images && images.length>0 && images.map(el=>{
                var img = 'http://localhost:3001/' + el.photoPath.replace(/\\/g, "").replace("uploads", "");

                return(
                  <div key={el.id} className="item-images-upload"> 
                  <div className="back-gr"></div>
                  <span onClick={()=>onDeleteImage(el)} className="btn_remove"></span>
                    <img src={img}></img>
                  </div>
                )
              })
            }
        </div>
        </div>
      </div>
    );
  }

  UpLoadForm.prototype = {
    className: PropTypes.string
  }
  UpLoadForm.defaultProp = {
    className:''
  }
  const mapStateSelectToProps = createStructuredSelector({
    files: makeSelectFiles() || '',
    state: makeHomeState()
  });
  export function mapDispatchToProps(dispatch, higherProps){
    return{
      onHandleUpload: (files)=>{
        dispatch(handleUpload(files))
      },
      onDeleteImage: (image)=>{
        dispatch(deleteImage(image))
      },
      higherProps: {...higherProps}
    }
  }

  const withConnect = connect(
    mapStateSelectToProps,
    mapDispatchToProps,

  );

  export default compose(
    withConnect,
    memo,
  )(UpLoadForm);
