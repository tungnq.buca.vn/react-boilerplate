/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import { ON_CHANGE_INPUT, UPDATE_IMAGES, ON_DELETE_IMAGE } from './constants';
import { combineReducers } from "redux";

// The initial state of the App
export const initialState = {
  files: [],
  images: [],

};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>{
  switch (action.type) {
    case ON_CHANGE_INPUT:
      return{
        ...state,
        files: action.files
      }
    case UPDATE_IMAGES:
      return{
        ...state,
        images: action.images
      }
    case ON_DELETE_IMAGE:
      let {images} = state
      images = images.filter(el=>{
        return el.id != action.item.id
      })
      return{
        ...state,
        images
      }
    default:
      return {
        ...state
      }
  }
}


  export default combineReducers({
    homeState: homeReducer
  });

  
// export default homeReducer;
