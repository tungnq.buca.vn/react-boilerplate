import React from 'react';
//Redux
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Provider } from 'react-redux';
import Main from './main'
import  reducers  from './reducer';
//Redux saga
import createSagaMiddleware from 'redux-saga';
import rootSaga from './saga'; 

const sagaMiddleware = createSagaMiddleware();
let store = createStore(reducers, composeWithDevTools(applyMiddleware(sagaMiddleware)));
// import store from './store'

const App = (props) => {
    return(
        <Provider store={store}>
            <Main {...props}/>
        </Provider>
    )
}
sagaMiddleware.run(rootSaga);
export default App;