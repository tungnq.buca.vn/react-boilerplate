
import { call, put, select, takeLatest } from 'redux-saga/effects';
import axios from 'axios'
import { ON_CHANGE_INPUT } from './constants';
import request from 'utils/request';
import { makeSelectFiles } from './selectors';
import { upateFile } from './actions';

/**
 * Root saga manages watcher lifecycle
 */

export function* uploadFile() {
    
    try {
        const filesList = yield select(makeSelectFiles());
        const files = [...filesList]
        let formData = new FormData();  
        files.length>0 && files.forEach(el => {
            formData.append('photo', el);
        });
        const func = new Promise(reslove=>{
             axios({
                method: "post",
                url: 'http://localhost:3001/photos/upload',
                data: formData,
                config: { headers: { "Content-Type": "multipart/form-data" } }
            }).then(res=>{
            //   console.log('res', res)
                if(!res || !res.data || !res.data.status) return
                reslove(res.data.data)
            })
            
        }) 
        // const dataFiles = []
        let arrImages = []
        yield func.then(data=>{
            arrImages = data
        })
        console.log('arrImages', arrImages)
        yield put(upateFile(arrImages))
    } catch (err) {
      console.log('err', err)
    }
}


export default function* WatchesActions() {
    yield takeLatest(ON_CHANGE_INPUT, uploadFile);
}
