/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.homeState || initialState;


const makeSelectFiles = () =>
  createSelector(
    selectHome,
    homeState => homeState.files,
  );
const makeHomeState = () =>
  createSelector(
    selectHome,
    homeState => homeState,
  );
  
export { selectHome, makeSelectFiles, makeHomeState};
