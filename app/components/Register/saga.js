/**
 * Gets the repositories of the user from Github
 */

import { call, put, select, takeLatest } from 'redux-saga/effects';
import { ADD_USER, ONSUBMIT } from './constants';
import { checkLogin } from './actions';

import request from 'utils/request';
import { makeSelectEmail, makeSelectPassword } from './selectors';

/**
 * Github repos request/response handler
 */
export function* getRepos() {
  // Select username from store
  const email = yield select(makeSelectEmail());
  const password = yield select(makeSelectPassword());

  const requestURL = `http://localhost:4000/users`;

  try {
    // Call our request helper (see 'utils/request')
    const result = yield fetch(requestURL, {
      method: 'POST',
      body: JSON.stringify({
        email,
        password
      })
    })

    // yield put(reposLoaded(repos, email));
  } catch (err) {
    alert('err')
  }
}
export function* addUser() {
  // Select email from store
  const email = yield select(makeSelectEmail());
  const password = yield select(makeSelectPassword());
  const requestURL = 'http://localhost:3001/users';
  let requestOptions = {
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache', 
            credentials: 'same-origin',
            headers: { 
                          'Content-Type': 'application/json',
                          'Authorization': 'Bearer 2606951',
              },
            redirect: 'follow',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify({
              email,
              password
            })
  };
  try {
    // Call our request helper (see 'utils/request')
    const result = yield call(request, requestURL, requestOptions);
    yield put(checkLogin(result));
  } catch (err) {
    console.log('err', err)
  }
}
export function* login(){
    // Select email from store
    const email = yield select(makeSelectEmail());
    const password = yield select(makeSelectPassword());
    const requestURL = 'http://localhost:3001/auth/login';
    let requestOptions = {
              method: 'POST',
              mode: 'cors',
              cache: 'no-cache', 
              credentials: 'same-origin',
              headers: { 
                            'Content-Type': 'application/json',
                            'Authorization': 'Bearer 2606951',
              },
              redirect: 'follow',
              referrerPolicy: 'no-referrer',
              body: JSON.stringify({
                email,
                password
              })
    };
    try {
      // Call our request helper (see 'utils/request')
      const result = yield call(request, requestURL, requestOptions);
      yield put(checkLogin(result));
    } catch (err) {
      console.log('err', err)
    }
}

/**
 * Root saga manages watcher lifecycle
 */
export default function* WatchesActions() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // It will be cancelled automatically on component unmount
  // yield takeLatest(ADD_USER, addUser);
  yield takeLatest(ONSUBMIT, login);
  yield takeLatest(ADD_USER, addUser);
}
