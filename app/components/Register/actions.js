/*
 * Home Actions
 *
 * Actions change things in your application
 * Since this boilerplate uses a uni-directional data flow, specifically redux,
 * we have these actions which are the only way your application interacts with
 * your application state. This guarantees that your state is up to date and nobody
 * messes it up weirdly somewhere.
 *
 * To add a new Action:
 * 1) Import your constant
 * 2) Add a function like this:
 *    export function yourAction(var) {
 *        return { type: YOUR_ACTION_CONSTANT, var: var }
 *    }
 */

import { ONSUBMIT , ON_SWITCH_NEW_ACCOUNT, ON_LOG_IN, CHECK_LOG_IN, ON_CHANGE_REGISTER, TRY_LOG_IN_AGAIN, ADD_USER } from './constants';

/**
 * Changes the input field of the form
 *
 * @param  {string} email The new text of the input field
 *
 * @return {object} An action object with a type of ONSUBMIT
 */


export function submitForm(email, password) {
  return {
    type: ONSUBMIT,
    email,
    password,
  };
}
export function changeRegister(email, password, username) {
  return {
    type: ON_CHANGE_REGISTER,
    email,
    password,
    username
  };
}
export function switchCreateNewAccount(isLogin) {
  return {
    type: ON_SWITCH_NEW_ACCOUNT,
    isLogin: !isLogin
  };
}
export function logIn(isLogin) {
  return {
    type: ON_LOG_IN
  };
}
export function checkLogin(result) {
  const isLoginSuccess = result.success
  return {
    type: CHECK_LOG_IN,
    isLoginSuccess
  };
}
export function tryLoginAgain() {
  return {
    type: TRY_LOG_IN_AGAIN,
    isOpenErrBox: true
  };
}
export function addUser() {
  return {
    type: ADD_USER
  };
}
