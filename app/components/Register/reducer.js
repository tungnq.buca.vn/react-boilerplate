/*
 * HomeReducer
 *
 * The reducer takes care of our data. Using actions, we can
 * update our application state. To add a new action,
 * add it to the switch statement in the reducer function
 *
 */

import { ONSUBMIT, ON_SWITCH_NEW_ACCOUNT, CHECK_LOG_IN , ON_CHANGE_REGISTER, TRY_LOG_IN_AGAIN} from './constants';
import { combineReducers } from "redux";

// The initial state of the App
export const initialState = {
  email: '',
  password: '',
  username: '',
  isLogin: true,
  isOpenErrBox: false
};

/* eslint-disable default-case, no-param-reassign */
const homeReducer = (state = initialState, action) =>{
  switch (action.type) {
    case ONSUBMIT:
      return{
        ...state,
        email: action.email,
        password: action.password,

      }
    case ON_CHANGE_REGISTER:
      return{
        ...state,
        email: action.email,
        password: action.password,
        username: action.username

      }
    case CHECK_LOG_IN:
      const isOpenErrBox = !action.isLoginSuccess
      return{
        ...state,
        isLoginSuccess: action.isLoginSuccess,
        isOpenErrBox

      }
    case ON_SWITCH_NEW_ACCOUNT:
      return {
        ...state,
        isLogin: action.isLogin
      }
    case TRY_LOG_IN_AGAIN:
      return {
        ...state,
        isOpenErrBox: false
      }
    default:
      return {
        ...state
      }
  }
}





  export default combineReducers({
    homeState: homeReducer
  });

  
// export default homeReducer;
