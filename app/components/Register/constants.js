/*
 * HomeConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const ONSUBMIT = 'ONSUBMIT';
export const ADD_USER = 'ADD_USER';
export const ON_SWITCH_NEW_ACCOUNT  = 'ON_SWITCH_NEW_ACCOUNT ';
export const ON_LOG_IN  = 'ON_LOG_IN ';
export const CHECK_LOG_IN  = 'CHECK_LOG_IN ';
export const ON_CHANGE_REGISTER  = 'ON_CHANGE_REGISTER';
export const TRY_LOG_IN_AGAIN  = 'TRY_LOG_IN_AGAIN';
