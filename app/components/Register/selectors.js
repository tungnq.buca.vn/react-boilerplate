/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectHome = state => state.homeState || initialState;


const makeSelectEmail = () =>
  createSelector(
    selectHome,
    homeState => homeState.email,
  );
const makeSelectUsername = () =>
  createSelector(
    selectHome,
    homeState => homeState.username,
  );
const makeSelectPassword = () =>
  createSelector(
    selectHome,
    homeState => homeState.password,
  );
const makeisLogin = () =>
  createSelector(
    selectHome,
    homeState => homeState.isLogin,
  );
const makeIsLoginSuccess = () =>
  createSelector(
    selectHome,
    homeState => homeState.isLoginSuccess,
  );
const makeisOpenErrBox = () =>
  createSelector(
    selectHome,
    homeState => homeState.isOpenErrBox,
  );
export { selectHome, makeSelectEmail, makeSelectPassword , makeisLogin, makeSelectUsername, makeIsLoginSuccess, makeisOpenErrBox};
