  /*
  * HomePage
  *
  * This is the first thing users see of our App, at the '/' route
  */

  import React, { memo, useRef, Fragment, useState } from 'react';
  import PropTypes from 'prop-types';
  import { FormattedMessage } from 'react-intl';
  // import { firestoreConnect } from 'react-redux-firebase'
  import { connect } from 'react-redux';
  import { compose } from 'redux';
  import { createStructuredSelector } from 'reselect';
  import ConfirmBox from "commons/confirmBox";
  import { makeSelectEmail , makeSelectPassword, makeisLogin, makeSelectUsername, makeIsLoginSuccess, makeisOpenErrBox  } from './selectors';
  import { changeRegister, switchCreateNewAccount, logIn, submitForm, addUser, tryLoginAgain } from './actions';
  import CenteredSection from './CenteredSection';
  import Form from './Form';
  import Section from './Section';
  import messages from './messages';
  import './index.scss';
  import {emailRegex, nameRegex, passWordRegex} from 'utils/regex'



  export function HomePage({ onSubmitForm, OnswitchCreateNewAccount, onLogIn, isLogin, onChangeCreateNew, 
    email, password, username, isLoginSuccess, isOpenErrBox, onTryLoginAgain, onAddUser, propsHigher }) {
    const refInputEmail = useRef();
    const refInputPassword = useRef();
    const refInputUserName = useRef();
    const validateEmail = new RegExp(emailRegex,'g').test(email)
    const validateName = new RegExp(nameRegex,'g').test(username)
    const validatePassword = new RegExp(passWordRegex,'g').test(password)
    const disabled = !validateEmail || !validateName || !validatePassword
    return (
      <Fragment>
        <div className="wrapper-popup-sign-in">
          <CenteredSection>
            <p>
              {
                isLogin ? <FormattedMessage {...messages.titleLognin} />
                  :
                  <Fragment>
                    <button onClick={()=>OnswitchCreateNewAccount(isLogin)}>turn back</button>
                    <FormattedMessage {...messages.titleRegister} />
                  </Fragment>
              }
            </p>
          </CenteredSection>
          <Section>
            <div className="form-signin">
              <div className="wrap-form">
                <div className="content-form">
                  {
                    isLogin ?
                      <Fragment>
                        <Form onSubmit={() => onSubmitForm(event, refInputEmail.current && refInputEmail.current.value, refInputPassword.current && refInputPassword.current.value)}>
                          <label>email</label>
                          <input
                            id="username"
                            type="text"
                            ref={refInputEmail}
                          />
                        </Form>
                        <Form onSubmit={() => onSubmitForm(event, refInputEmail.current && refInputEmail.current.value, refInputPassword.current && refInputPassword.current.value)}>
                          <label>password</label>
                          <input
                            id="username"
                            type="text"
                            ref={refInputPassword}
                          />
                        </Form>
                      </Fragment>
                      :
                      <div className="content-register-new-account">
                        <Form >
                          <label>username</label>
                          <input
                            id="username"
                            type="text"
                            ref={refInputUserName}
                            onChange={()=>onChangeCreateNew(refInputEmail.current && refInputEmail.current.value, refInputPassword.current && refInputPassword.current.value , refInputUserName.current && refInputUserName.current.value)}
                          />
                          {
                            username && !validateName && <div className="validate-input">Tên chỉ được phép chứa các chữ cái và chữ số, và không được kết thúc bằng khoảng trắng</div>
                          }
                          
                        </Form>
                          <Form>
                          <label>email</label>
                          <input
                            id="username"
                            type="text"
                            ref={refInputEmail}
                            onChange={()=>onChangeCreateNew(refInputEmail.current && refInputEmail.current.value, refInputPassword.current && refInputPassword.current.value , refInputUserName.current && refInputUserName.current.value)}
                          />
                            {
                            email && !validateEmail && <div className="validate-input">Định dạng Email không hợp lệ</div>
                          }
                        </Form>
                        <Form>
                          <label>password</label>
                          <input
                            id="username"
                            type="password"
                            ref={refInputPassword}
                            onChange={()=>onChangeCreateNew(refInputEmail.current && refInputEmail.current.value, refInputPassword.current && refInputPassword.current.value , refInputUserName.current && refInputUserName.current.value)}
                          />
                            {
                            password && !validatePassword && <div className="validate-input">Password chứa kỹ tự viết hoa, viết thường , số và trên 8 ký tự</div>
                          }
                        </Form>
                      </div>
                }

                  {
                    isLogin ? <div onClick={() => onLogIn(event, refInputEmail.current && refInputEmail.current.value, refInputPassword.current && refInputPassword.current.value)} className="button-signin">Sign in</div>
                      : <div   className= {disabled ? "button-signin disable-button" : 'button-signin' } onClick={()=>onAddUser()}>Create new</div>
                  }

                </div>
                {
                  isLogin ?
                    <div className="create-new-account">
                      <span>New to BuCa?</span> <span className="click-create-new" onClick={() => OnswitchCreateNewAccount(isLogin)}>Create new account</span>
                    </div>
                    :
                    null
                }
              </div>
            </div>
          </Section>
        </div>
        <ConfirmBox callback ={()=>onTryLoginAgain()} open = {typeof isLoginSuccess !== 'undefined' && isOpenErrBox || false} />
      </Fragment>
    );
  }

  HomePage.propTypes = {
    email: PropTypes.string,
    username: PropTypes.string,
    password: PropTypes.string,
    onSubmitForm: PropTypes.func,
    isLogin: PropTypes.bool || func,
    isOpenErrBox: PropTypes.bool
  };
  const mapState = createStructuredSelector({
    email: makeSelectEmail() || '',
    password: makeSelectPassword() || '',
    username: makeSelectUsername() || '',
    isLogin: makeisLogin() || true,
    isOpenErrBox: makeisOpenErrBox() || false,
    isLoginSuccess :  makeIsLoginSuccess() || undefined 
  });

  export function mapDispatchToProps(dispatch, props) {
    return {
      onChangeCreateNew: (email, password, username)=>{
        dispatch(changeRegister(email, password, username))
      },
      onSubmitForm: (event, email, password) => {
        if (event !== undefined && event.preventDefault) event.preventDefault();
        dispatch(submitForm(email, password));
        // dispatch(addUser());
      },
      onLogIn: (event, email, password) => {
        if (event !== undefined && event.preventDefault) event.preventDefault();
        dispatch(submitForm(email, password));
        dispatch(logIn());
      },
      OnswitchCreateNewAccount: (isLogin) => {
        dispatch(switchCreateNewAccount(isLogin))
      },
      onTryLoginAgain: ()=>{
        dispatch(tryLoginAgain())
      },
      onAddUser: ()=>{
        dispatch(addUser())
      },
      propsHigher: {...props}
    };;
  }

  const withConnect = connect(
    mapState,
    mapDispatchToProps
  );

  export default compose(
    withConnect,
    memo,
  )(HomePage);
