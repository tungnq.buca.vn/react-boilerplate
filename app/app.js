/**
 * app.js
 *
 * This is the entry file for the application, only setup and boilerplate
 * code.
 */

// Needed for redux-saga es6 generator support
import '@babel/polyfill';
// Import all the third party stuff
import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import FontFaceObserver from 'fontfaceobserver';
import history from 'utils/history';
import 'sanitize.css/sanitize.css';
import { Helmet } from 'react-helmet';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
// Import root app
// import App from 'containers/App';
import RegisterPopUp from 'components/Register'
import Table from 'components/Table'
import PopupTrigger from "commons/popuptrigger";
import FormStepByStep from "commons/formstepbystep";
import UloadFile from "components/uploadfile";
import NotFoundPage from 'containers/NotFoundPage/Loadable';
// Import Language Provider
import LanguageProvider from 'containers/LanguageProvider';
import Button from '@material-ui/core/Button';
// Load the favicon and the .htaccess file
import '!file-loader?name=[name].[ext]!./images/favicon.ico';
import 'file-loader?name=.htaccess!./.htaccess'; // eslint-disable-line import/extensions

import configureStore from './configureStore';

// Import i18n messages
import { translationMessages } from './i18n';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
});

// Create redux store with history
const initialState = {};
const store = configureStore(initialState, history);
const MOUNT_NODE = document.getElementById('app');

// const render = messages => {
//   ReactDOM.render(
//     <Provider store={store}>
//       <LanguageProvider messages={messages}>
//         <ConnectedRouter history={history}>
//           <App />
//         </ConnectedRouter>
//       </LanguageProvider>
//     </Provider>,
//     MOUNT_NODE,
//   );
// };


const columns = [
  {
    id: 'fullName',
    label: 'fullName',
  },
  { id: 'mobile', label: 'mobile' },
  { id: 'email', label: 'email', isEmail: true },
  { id: 'dateOfBirth', label: 'dateOfBirth', isDateFormat: true },
  { id: 'bloodGroup', label: 'bloodGroup' },
  { id: 'address', label: 'address', isComboBox: true },
]
const _options = {
  columns: columns,
  isEdit: true,
  isDelete: true,
  multiDelete: true,
  editInline: true,
  rowPerPage: 4,
  limit: 100,
  isPagination: true,
  url: 'http://localhost:3967/api/DCandidate',
  urlPost: 'http://localhost:3967/api/DCandidate/add'
}


const render = messages => {
  ReactDOM.render(
    <Fragment>
      <Router>
        <Helmet
          titleTemplate="%s - React.js Boilerplate"
          defaultTitle="React.js Boilerplate"
        >
          <meta name="description" content="A React.js Boilerplate application" />
        </Helmet>
         <PopupTrigger
          renderPopup={({ handleOpen, handleClose }) => <RegisterPopUp />}>
          {
            ({ handleOpen }) => (
              <button onClick={handleOpen}>log in</button>
            )
          }
        </PopupTrigger> 
        <PopupTrigger
          renderPopup={({ handleOpen, handleClose }) => <UloadFile />}>
          {
            ({ handleOpen }) => (
              <button onClick={handleOpen}>upload file</button>
            )
          }
        </PopupTrigger>
        <PopupTrigger
          renderPopup={({ handleOpen, handleClose }) => <FormStepByStep />}>
          {
            ({ handleOpen }) => (
              <button onClick={handleOpen}>form step</button>
            )
          }
        </PopupTrigger>
        <Button to=""><Link to="/table">table</Link></Button>
        <Switch>
          <Route path="/table"> <Table  options = {_options}/> </Route>
        </Switch>
      </Router>
    </Fragment>,
    MOUNT_NODE,
  );
};

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['./i18n', 'containers/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render(translationMessages);
  });
}

// Chunked polyfill for browsers without Intl support
if (!window.Intl) {
  new Promise(resolve => {
    resolve(import('intl'));
  })
    .then(() =>
      Promise.all([
        import('intl/locale-data/jsonp/en.js'),
        import('intl/locale-data/jsonp/de.js'),
      ]),
    ) // eslint-disable-line prettier/prettier
    .then(() => render(translationMessages))
    .catch(err => {
      throw err;
    });
} else {
  render(translationMessages);
}

// Install ServiceWorker and AppCache in the end since
// it's not most important operation and if main code fails,
// we do not want it installed
if (process.env.NODE_ENV === 'production') {
  require('offline-plugin/runtime').install(); // eslint-disable-line global-require
}
